import $ from "jquery";
import createSquareSwiper from "./components/square-swiper";

// square swipre
const firstSwiper = createSquareSwiper(
  ".page-main__swiper-with-text .swiper-square"
);
const secondSwiper = createSquareSwiper(
  ".page-main__text-with-swiper .swiper-square"
);

//TIP: for hide unnecessary elements now
[
  ".page-main__people",
  // ".page-main__swiper-with-text",
  // ".page-main__text-with-swiper",
  // ".header",
].forEach((selector) => {
  $(selector).addClass("hidden");
});
