// const $swiper = ".swiper-square";
import $ from "jquery";
import Swiper, { Navigation } from "swiper";

Swiper.use([Navigation]);

export default function createSquareSwiper(path: string) {
  let swiper = new Swiper(path + " .swiper-container", {
    loop: true,
    speed: 400,

    breakpoints: {
      "320": {
        centeredSlides: true,
        slidesPerView: 1,
        spaceBetween: 0,
      },
      "480": {
        centeredSlides: true,
        slidesPerView: "auto",
        spaceBetween: 30,
      },
      "1024": {
        centeredSlides: false,
        spaceBetween: 0,
        slidesPerView: 1,
      },
    },

    navigation: {
      nextEl: path + " .swiper-next",
      prevEl: path + " .swiper-prev",
    },
    // preloadImages: false,
    // Enable lazy loading
    // lazy: true,
    // And if we need scrollbar
    //   scrollbar: {
    //     el: ".swiper-scrollbar",
    //   },
  });
  return swiper;
}
