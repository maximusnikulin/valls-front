import { fullpage } from "jquery";

export default () => {
  $(".fullpage").fullpage({
    //options here
    autoScrolling: true,
    scrollHorizontally: true,
  });
};
