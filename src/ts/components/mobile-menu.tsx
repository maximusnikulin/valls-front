import $ from "jquery";

const $backdrop = $(".mobile-menu__backdrop");
const $menu = $(".mobile-menu");
const $toggler = $(".burger");

$toggler.on("click", function () {
  console.log("click burger 123");
  $menu.toggleClass("opened");
  $("body").toggleClass("no-scroll mobile-open");
  $(this).toggleClass("close");
});

$backdrop.on("click", () => {
  console.log("click backdrop");
  $menu.removeClass("opened");
  $toggler.removeClass("close");
});
