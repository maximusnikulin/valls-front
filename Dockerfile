FROM node:18-alpine

WORKDIR /app
COPY ./src/index.js /app/index.js

CMD ["node", "./index.js"]